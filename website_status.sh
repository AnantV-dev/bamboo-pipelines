#!/bin/bash

if curl -s --head  --request GET $WEBSITE | grep "301" > /dev/null; then 
   echo "This site is working fine"
else
   echo "This site is not working fine"
fi

#Download a notepad ++ setup to generate an artifact.

curl https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v8.5.4/npp.8.5.4.portable.x64. --output npp.8.5.4.portable.x64.
